<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'description' => implode("\n\n", $faker->paragraphs(rand(3, 6))) ,
        'author' => $faker->name,
        'image' => "default.png",
    ];
});