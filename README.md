# Rss Using Pusher

News feed  app with its endpoints

-user can login,register and post a new text or photo

-all users can see all posts from other users at real time manner

## Learning Pusher

https://pusher.com/docs

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability , please send an e-mail to Mahmoud Hammam via [mahmoudhammam517@gmail.com](mailto:mahmoudhammam517@gmail.com).

## Up and Running

create pusher account from https://dash.pusher.com/authenticate/register

git clone rep

open rep folder via terminal

composer install

cp .env.example .env for linux

copy .env.example .env for windows

Open .env and add your DATABASE and PUSHER CREDENTIALS 

DB_DATABASE=xxxxxxxxx

DB_USERNAME=xxxxxxxxx

DB_PASSWORD=xxxxxxxxx

PUSHER_APP_ID=xxxxxxxxx

PUSHER_KEY=xxxxxxxxxxx

PUSHER_SECRET=xxxxxxxxxx

PUSHER_APP_CLUSTER=

php artisan key:generate

php artisan vendor:publish

composer dumpautoload

php artisan migrate

php artisan db:seed

## open app url and login with mahmoudhammam517@gmail.com , secret

## open /events.rss url on your installed app to see Rss xml file result 