<?php

namespace App;

use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;
use Illuminate\Database\Eloquent\Model;
use Zttp\Zttp;

class Event extends Model implements Feedable
{
    protected $guarded = [];
	
	public function toFeedItem()
    {
        return FeedItem::create()
            ->id($this->id)
            ->title($this->title)
            ->summary($this->description)
            ->updated($this->updated_at)
            ->link($this->link)
            ->author($this->author);
    }
    public static function getFeedItems()
    {
        return static::all();
    }
    public function getLinkAttribute()
    {
        return route('events.show', $this);
    }
	
	 public static function moderate($text)
    {
        $response = Zttp::withoutVerifying()->post("https://commentator.now.sh", [
            'comment' => $text,
            'limit' => -3,
        ])->json();
        if ($response['commentate']) {
            abort(400, "text not allowed");
        }
    }
	
}
