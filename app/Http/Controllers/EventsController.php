<?php

namespace App\Http\Controllers;
use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class EventsController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');       
    
    }
	
	public function show(Request $request, Event $event)
    {
        return $event;
    }
}
