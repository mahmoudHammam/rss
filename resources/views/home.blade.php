@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Events</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                   <div>
	<div>
    <h3>New Event</h3>
    <form onsubmit="addEvent(event);">
        <input type="text" placeholder="Add a title" name="title" id="title" required />
        <input type="text" placeholder="Author name" name="author" id="authorname" required />
        <input type="text" hidden name="image" id="image" value="default.png" />
		<br />
		<br />
		<textarea type="text" rows="7" placeholder="Add a description" name="description" id="description" required></textarea>
        <button id="addEventBtn" hidden >Create</button>
    </form>
	</div>
	<div class="alert" id="message" style="display: none"></div>
   <form method="post" id="upload_form" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="right"><label>Select File for Upload</label></td>
       <td width="30"><input type="file" name="select_file" id="select_file" /></td>
       <td width="30%" align="left"><input type="submit" name="upload" id="upload" class="btn btn-primary" hidden value="Upload"></td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30"><span class="text-muted">jpg, png, gif</span></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>
   <br />
   <button id="eventcreate" onclick="fire_create_event();">Create Event</button>
   <span id="uploaded_image"></span>
    <div class="alert" id="alert" style="display: none;"></div>
    <br />
	<hr />
    <div id="events">
        @foreach($events as $event)
            <div>
                <h4>{{ $event->title }}</h4>
				<small>{{ $event->author }}</small>
				<img src="{{asset('/images/'.$event->image)}}" class="img-thumbnail"/>
                <br />
                {{ $event->description }}
            </div>
			<hr />
        @endforeach
    </div>
</div>



                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>
    function fire_create_event(){
		if( document.getElementById("select_file").files.length == 0 ){
		   $('#eventcreate').attr('disabled', 'disabled');
           $("#addEventBtn").click();
        }
		else{
		   $('#eventcreate').attr('disabled', 'disabled');
		   $("#upload").click();
		}
	}
    function displayEvent(data) {
        let $event = $('<div>').text(data['description']).prepend($('<img src="{{asset("/images/")}}/'+data['image']+'" class="img-thumbnail"/>'));
		$event.prepend($('<small>').html(data['author'] + "<br>"));
		$event.prepend($('<h3>').html(data['title'] + "<h3>"));
		$event.append($('<hr />'));
        $('#events').prepend($event);
		/*$('#title').val("");
        $('#description').val("");
        $('#authorname').val("");*/
    }

    function addEvent (event) {

        function showAlert(message) {
            let $alert = $('#alert');
            $alert.text(message).show();
            setTimeout(() => $alert.hide(), 4000);
        }

        event.preventDefault();
        let data = {
            title: $('#title').val(),
            description: $('#description').val(),
            author: $('#authorname').val(),
            image: $('#image').val(),
        };
        fetch("{{url('/events')}}", {
            body: JSON.stringify(data),
            credentials: 'same-origin',
            headers: {
                'content-type': 'application/json',
                'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
                'x-socket-id': window.socketId
            },
            method: 'POST',
            mode: 'cors',
        }).then(response => {
            $('#eventcreate').removeAttr('disabled');
            if (response.ok) {
                displayEvent(data);
                showAlert('Event posted!');
            } else {
                showAlert('Your Event was not approved for posting. Please be nicer :)');
            }
        })
    }
</script>

<script src="https://js.pusher.com/4.2/pusher.min.js"></script>
<script>
    var socket = new Pusher("63ac76ee0e8e9359cfbf", {
        cluster: 'ap2',
    });
    socket.connection.bind('connected', function() {
        window.socketId = socket.connection.socket_id;
    });
    socket.subscribe('events')
        .bind('new-event',displayEvent);
</script>

<script>
$(document).ready(function(){

 $('#upload_form').on('submit', function(event){
  event.preventDefault();
  $.ajax({
   url:"{{ url('uploadimage') }}",
   method:"POST",
   data:new FormData(this),
   dataType:'JSON',
   contentType: false,
   cache: false,
   processData: false,
   success:function(data)
   {
    if(data.message=="Image Upload Successfully"){
	  $('#message').css('display', 'block');
      $('#message').html(data.message);
      $('#message').addClass(data.class_name);
	  $('#image').val(data.uploaded_image);
	  $("#addEventBtn").click();	
	}
	else{
	  $('#message').css('display', 'block');
      $('#message').html(data.message);
      $('#message').addClass(data.class_name);
	  $('#eventcreate').removeAttr('disabled');
	}
   }
  })
 });

});
</script>

@endsection
